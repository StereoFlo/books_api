<?php

declare(strict_types = 1);

namespace App\Exception;

use Exception;

class HandlerCommonException extends Exception
{
    /**
     * @var array<array<int, string>>
     */
    private array $exceptions;

    /**
     * @param array<array<int, string>> $exceptions
     */
    public function __construct(array $exceptions)
    {
        parent::__construct();
        $this->exceptions = $exceptions;
    }

    /**
     * @return array<array<int, string>>
     */
    public function getErrors(): array
    {
        return $this->exceptions;
    }
}
