<?php

declare(strict_types = 1);

namespace App\State;

use App\Exception\HandlerCommonException;
use App\Exception\ValidationException;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Throwable;
use function array_map;
use function array_shift;
use function count;
use function method_exists;

class State
{
    private MessageBusInterface $bus;

    private ValidatorInterface $validator;

    /**
     * State constructor.
     */
    public function __construct(MessageBusInterface $bus, ValidatorInterface $validator)
    {
        $this->bus       = $bus;
        $this->validator = $validator;
    }

    /**
     * @throws HandlerCommonException
     *
     * @return mixed
     */
    public function update(MessageInterface $command)
    {
        return $this->dispatch($command);
    }

    /**
     * @throws HandlerCommonException
     *
     * @return mixed
     */
    public function query(MessageInterface $query)
    {
        return $this->dispatch($query);
    }

    /**
     * @param MessageInterface $message The message or the message pre-wrapped in an envelope
     *
     * @throws HandlerCommonException
     *
     * @return mixed|null
     */
    private function dispatch(MessageInterface $message)
    {
        $this->validate($message);

        try {
            $envelope = $this->bus->dispatch($message);
            $result   = $envelope->last(HandledStamp::class);

            if (empty($result) || !method_exists($result, 'getResult')) {
                return null;
            }

            return $result->getResult();
        } catch (HandlerFailedException $e) {
            $this->throwHandlerException($e);
        }
    }

    private function validate(MessageInterface $message): void
    {
        $violations = $this->validator->validate($message);
        if ($violations->count()) {
            $errors = [];

            /** @var ConstraintViolation $constraintViolation */
            foreach ($violations as $constraintViolation) {
                $errors[$constraintViolation->getPropertyPath()] = $constraintViolation->getMessage();
            }

            throw new ValidationException($errors);
        }
    }

    /**
     * @throws HandlerCommonException
     */
    private function throwHandlerException(HandlerFailedException $e): void
    {
        $exceptions = $e->getNestedExceptions();

        if (1 < count($exceptions)) {
            $errors = array_map(function (Throwable $throwable): array {
                return [
                    (int) $throwable->getCode() => $throwable->getMessage(),
                ];
            }, $exceptions);

            throw new HandlerCommonException($errors);
        }

        if (1 === count($exceptions)) {
            throw array_shift($exceptions);
        }

        throw $e;
    }
}
