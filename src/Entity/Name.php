<?php

declare(strict_types = 1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
class Name
{
    /**
     * @ORM\Column(name="label_ru")
     */
    private string $ru;

    /**
     * @ORM\Column(name="label_en")
     */
    private string $en;

    public function __construct(string $ru, string $en)
    {
        $this->ru = $ru;
        $this->en = $en;
    }

    public function getRu(): string
    {
        return $this->ru;
    }

    public function getEn(): string
    {
        return $this->en;
    }
}
