<?php

declare(strict_types = 1);

namespace App\Mapper;

use App\Entity\Author;
use function array_map;

class AuthorMapper
{
    /**
     * @return array<string, mixed>
     */
    public function map(Author $author): array
    {
        return [
            'id'      => $author->getId(),
            'name_ru' => $author->getNames()->getRu(),
            'name_en' => $author->getNames()->getEn(),
        ];
    }

    /**
     * @param Author[] $authors
     *
     * @return array<array<string, mixed>>
     */
    public function mapCollection(array $authors): array
    {
        return array_map([$this, 'map'], $authors);
    }
}
