<?php

declare(strict_types = 1);

namespace App\Entity;

use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="books")
 */
class Book
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned":true})
     */
    private int $id;
    /**
     * @ORM\Embedded(class="App\Entity\Name")
     */
    private Name $names;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Author", inversedBy="books", cascade={"persist", "remove"})
     */
    private Collection $authors;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $updatedAt;

    /**
     * @param Author[] $authors
     */
    public function __construct(Name $names, array $authors = [])
    {
        $this->names   = $names;
        $this->authors = new ArrayCollection($authors);

        $this->createdAt = new DateTimeImmutable();
        $this->updatedAt = new DateTimeImmutable();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getNames(): Name
    {
        return $this->names;
    }

    public function getAuthors(): Collection
    {
        return $this->authors;
    }
}
