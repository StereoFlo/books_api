<?php

declare(strict_types = 1);

namespace App\Fixtures;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Name;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use function array_rand;

class BookFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $authors = [];
        for ($i = 0; $i < 10000; $i++) {
            $author    = new Author(new Name($this->getFaker('ru_RU')->name, $this->getFaker()->name));
            $authors[] = $author;
            $manager->persist($author);
        }
        $manager->flush();
        for ($i = 0; $i < 10000; $i++) {
            $auth = array_rand($authors);
            $book = new Book(new Name($this->getFaker('ru_RU')->colorName, $this->getFaker()->colorName), [$authors[$auth]]);
            $manager->persist($book);
        }
        $manager->flush();
    }

    private function getFaker(string $locale = Factory::DEFAULT_LOCALE): Generator
    {
        return Factory::create($locale);
    }
}
