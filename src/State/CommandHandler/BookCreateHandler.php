<?php

declare(strict_types = 1);

namespace App\State\CommandHandler;

use App\Entity\Book;
use App\Entity\Name;
use App\Repository\BookRepository;
use App\State\Command\BookCreateCommand;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class BookCreateHandler implements MessageHandlerInterface
{
    private BookRepository $bookRepo;

    public function __construct(BookRepository $bookRepo)
    {
        $this->bookRepo = $bookRepo;
    }

    public function __invoke(BookCreateCommand $command): void
    {
        $names = new Name($command->getNameRu(), $command->getNameEn());
        $book  = new Book($names);

        $this->bookRepo->set($book);
    }
}
