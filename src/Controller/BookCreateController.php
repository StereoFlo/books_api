<?php

declare(strict_types = 1);

namespace App\Controller;

use App\State\Command\BookCreateCommand;
use App\State\State;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/books", methods={"POST"})
 */
class BookCreateController
{
    public function __invoke(Request $request, State $state): JsonResponse
    {
        $state->update(new BookCreateCommand($request->get('name_ru', ''), $request->get('name_en', '')));

        return new JsonResponse(null, 204);
    }
}
