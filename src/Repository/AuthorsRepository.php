<?php

declare(strict_types = 1);

namespace App\Repository;

use App\Entity\Author;
use Doctrine\ORM\EntityManagerInterface;

class AuthorsRepository
{
    private EntityManagerInterface $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param int[] $ids
     *
     * @return Author[]
     */
    public function getByIds(array $ids): array
    {
        return $this->manager->getRepository(Author::class)->findBy(['id' => $ids]);
    }

    public function set(Author $author): void
    {
        $this->manager->persist($author);
        $this->manager->flush();
    }
}
