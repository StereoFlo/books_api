<?php

declare(strict_types = 1);

namespace App\Entity;

use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;

/**
 * @ORM\Entity()
 * @ORM\Table(name="authors")
 */
class Author
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned":true})
     */
    private int $id;
    /**
     * @ORM\Embedded(class="App\Entity\Name")
     */
    private Name $names;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Book", mappedBy="authors", cascade={"persist", "remove"})
     * @JoinTable(name="books_authors",
     *     joinColumns={
     *     @ORM\JoinColumn(name="book_id", referencedColumnName="id")},
     *     inverseJoinColumns={
     *         @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     *     }
     * )
     */
    private Collection $books;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $updatedAt;

    /**
     * @param Book[] $books
     */
    public function __construct(Name $names, array $books = [])
    {
        $this->names     = $names;
        $this->books     = new ArrayCollection($books);
        $this->createdAt = new DateTimeImmutable();
        $this->updatedAt = new DateTimeImmutable();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getNames(): Name
    {
        return $this->names;
    }
}
