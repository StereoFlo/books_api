<?php

declare(strict_types = 1);

namespace App\Repository;

use App\Entity\Book;
use App\State\Query\BookSearchQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

class BookRepository
{
    private EntityManagerInterface $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function set(Book $book): void
    {
        $this->manager->persist($book);
        $this->manager->flush();
    }

    public function getSearchCount(BookSearchQuery $query): int
    {
        return (new Paginator($this->getSearchQuery($query)))->count();
    }

    /**
     * @return Book[]
     */
    public function getSearchList(BookSearchQuery $query): array
    {
        return $this->getSearchQuery($query)
            ->setMaxResults($query->getLimit())
            ->setFirstResult($query->getOffset())
            ->getQuery()
            ->getResult();
    }

    private function getSearchQuery(BookSearchQuery $query): QueryBuilder
    {
        $qb = $this->manager->createQueryBuilder();

        return $qb
            ->select('book')
            ->from(Book::class, 'book')
            ->where('book.names.ru like \'%' . $query->getQuery() . '%\'')
            ->orWhere('book.names.en like \'%' . $query->getQuery() . '%\'');
    }
}
