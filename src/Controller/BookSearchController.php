<?php

declare(strict_types = 1);

namespace App\Controller;

use App\State\Query\BookSearchQuery;
use App\State\State;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/books", methods={"GET"})
 */
class BookSearchController
{
    public function __invoke(State $state, Request $request): JsonResponse
    {
        $res = $state->query(new BookSearchQuery(
            $request->get('query', ''),
            (int) $request->get('limit', 10),
            (int) $request->get('offset', 0),
        ));

        return new JsonResponse($res);
    }
}
