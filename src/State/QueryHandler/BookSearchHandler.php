<?php

declare(strict_types = 1);

namespace App\State\QueryHandler;

use App\Mapper\BookMapper;
use App\Repository\BookRepository;
use App\State\Query\BookSearchQuery;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class BookSearchHandler implements MessageHandlerInterface
{
    private BookRepository $bookRepo;
    private BookMapper $bookMapper;

    public function __construct(BookRepository $bookRepo, BookMapper $bookMapper)
    {
        $this->bookRepo   = $bookRepo;
        $this->bookMapper = $bookMapper;
    }

    /**
     * @return array<string, mixed>
     */
    public function __invoke(BookSearchQuery $query): array
    {
        $total = $this->bookRepo->getSearchCount($query);
        if (!$total) {
            return [
                'total' => 0,
                'items' => [],
            ];
        }

        $items = $this->bookRepo->getSearchList($query);

        return [
            'total' => $total,
            'items' => $this->bookMapper->mapCollection($items),
        ];
    }
}
