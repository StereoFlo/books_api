<?php

declare(strict_types = 1);

namespace App\Mapper;

use App\Entity\Book;
use function array_map;

class BookMapper
{
    private AuthorMapper $authorMapper;

    public function __construct(AuthorMapper $authorMapper)
    {
        $this->authorMapper = $authorMapper;
    }

    /**
     * @return array<string, mixed>
     */
    public function map(Book $book): array
    {
        return [
            'id'      => $book->getId(),
            'name_ru' => $book->getNames()->getRu(),
            'name_en' => $book->getNames()->getEn(),
            'authors' => $this->authorMapper->mapCollection($book->getAuthors()->toArray()),
        ];
    }

    /**
     * @param Book[] $books
     *
     * @return array<array<string, mixed>>
     */
    public function mapCollection(array $books): array
    {
        return array_map([$this, 'map'], $books);
    }
}
