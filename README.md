## Установка

```shell
composer install
```
миграции
```shell
php bin/console doctrine:migrations -n
```
фикстуры
```shell
php bin/console doctrine:fixtures:load -n
```

## Поддержка
#### Проврка cs

```shell
php vendor/bin/php-cs-fixer fix
```

#### PHPSTAN

```shell
php vendor/bin/phpstan analyse src --level=max --memory-limit=4G
```

## Роуты

создение книги
`POST /books`
```json
{
	"name_ru": "Название книги на русском",
	"name_en": "The name of the book in English"
}
```

Создание автора
`POST /autors`
```json
{
	"name_ru": "Имя автора на русском",
	"name_en": "Author's name in english"
}
```

на данный момент отсутствует логика связывания автора и книги.

поиск книги
`GET /books?query=some_query&limit=1&offset=0`
Ответ
```json
{
  "total": 193,
  "items": [
    {
      "id": 20073,
      "name_ru": "Золотой",
      "name_en": "DarkTurquoise",
      "authors": [
        {
          "id": 6444,
          "name_ru": "Пахомова Эльвира Ивановна",
          "name_en": "Miss Camylle Marvin Sr."
        }
      ]
    }
  ]
}
```
