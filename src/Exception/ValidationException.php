<?php

declare(strict_types = 1);

namespace App\Exception;

use InvalidArgumentException;
use Throwable;

class ValidationException extends InvalidArgumentException
{
    /**
     * @var array<string, mixed>
     */
    private array $errors;

    /**
     * ValidationException constructor.
     *
     * @param array<string, mixed> $errors
     */
    public function __construct(array $errors, string $message = '', int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->errors = $errors;
    }

    /**
     * @return array<string, mixed>
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
