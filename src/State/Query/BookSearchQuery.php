<?php

declare(strict_types = 1);

namespace App\State\Query;

use App\State\MessageInterface;
use Symfony\Component\Validator\Constraints as Assert;

class BookSearchQuery implements MessageInterface
{
    /**
     * @Assert\NotBlank()
     */
    private string $query;
    private int $limit;
    private int $offset;

    public function __construct(string $query, int $limit, int $offset)
    {
        $this->query  = $query;
        $this->limit  = $limit;
        $this->offset = $offset;
    }

    public function getQuery(): string
    {
        return $this->query;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }
}
