<?php

declare(strict_types = 1);

namespace App\State\Command;

use App\State\MessageInterface;
use Symfony\Component\Validator\Constraints as Assert;

class AuthorCreateCommand implements MessageInterface
{
    /**
     * @Assert\NotBlank()
     */
    private string $nameRu;

    /**
     * @Assert\NotBlank()
     */
    private string $nameEn;

    public function __construct(string $nameRu, string $nameEn)
    {
        $this->nameRu = $nameRu;
        $this->nameEn = $nameEn;
    }

    public function getNameRu(): string
    {
        return $this->nameRu;
    }

    public function getNameEn(): string
    {
        return $this->nameEn;
    }
}
