<?php

declare(strict_types = 1);

namespace App\State\CommandHandler;

use App\Entity\Author;
use App\Entity\Name;
use App\Repository\AuthorsRepository;
use App\State\Command\AuthorCreateCommand;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class AuthorCreateHandler implements MessageHandlerInterface
{
    private AuthorsRepository $authorsRepo;

    public function __construct(AuthorsRepository $authorsRepo)
    {
        $this->authorsRepo = $authorsRepo;
    }

    public function __invoke(AuthorCreateCommand $command): void
    {
        $names = new Name($command->getNameRu(), $command->getNameEn());
        $book  = new Author($names);

        $this->authorsRepo->set($book);
    }
}
